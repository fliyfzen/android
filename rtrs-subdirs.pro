################################################################################
#               __________                               __________            #
#               ___  ____/___________       ___   __________  /_  /_           #
#               __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           #
#               _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             #
#               /_____/  \___/ \____/       _____/ \____//_/  \__/             #
#                                                                              #
#     © 2016                                                                   #
#                                                                              #
################################################################################

CONFIG += ordered
TEMPLATE = subdirs

SUBDIRS = zbx \
		  rtrs_desk/ATR.pro \
		  rtrs-android


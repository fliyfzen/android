<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/Host.cpp" line="84"/>
        <source>W</source>
        <translation>Вт</translation>
    </message>
    <message>
        <location filename="../src/Host.cpp" line="87"/>
        <source>kW</source>
        <translation>КВт</translation>
    </message>
    <message>
        <location filename="../src/Host.cpp" line="99"/>
        <source>%1 dB</source>
        <translation>%1 дБ</translation>
    </message>
    <message>
        <location filename="../src/Host.cpp" line="125"/>
        <source> °C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/Host.cpp" line="183"/>
        <location filename="../src/Host.cpp" line="186"/>
        <source> V</source>
        <translation> В</translation>
    </message>
    <message>
        <location filename="../src/Host.cpp" line="189"/>
        <source> Wt</source>
        <translation> Вт</translation>
    </message>
    <message>
        <location filename="../src/Host.cpp" line="197"/>
        <source>%1 kWt</source>
        <translation>%1 КВт</translation>
    </message>
    <message>
        <source> kWt</source>
        <translation type="vanished"> КВт</translation>
    </message>
</context>
<context>
    <name>Zbx</name>
    <message>
        <location filename="../src/Zbx.cpp" line="141"/>
        <source>ERROR: URL invalid: %1</source>
        <translation>ОШИБКА: неверный URL: %1</translation>
    </message>
</context>
</TS>

/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#include "ModelHost.h"

#include <QDebug>

ModelHost::ModelHost( QObject * parent )
	: QAbstractListModel( parent )
{
}

void
ModelHost::append( const Host & host )
{
	beginInsertRows( QModelIndex(), m_hosts.size(), m_hosts.size() );
	m_hosts.append( host );
	endInsertRows();

	const QModelIndex index = createIndex( m_hosts.count() - 1, 0 );

	emit dataChanged( index, index );
}

int
ModelHost::rowCount( const QModelIndex & parent ) const			// override final
{
	if ( parent.isValid() )
		return 0;

	return m_hosts.count();
}

QVariant
ModelHost::data( const QModelIndex & index, int role ) const	// override final
{
	if ( ! index.isValid() )
		return QVariant();

	const int row = index.row();

	switch ( role ) {
		case Active:
			return m_hosts.at( row ).active.value;

		case AgentPing:
			return m_hosts.at( row ).agentPing.value;

		case Background:
			return m_hosts.at( row ).errorColor();

		case BackgroundActive:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).active );

		case BackgroundAgentPing:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).agentPing );

		case BackgroundCond1:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).cond1 );

		case BackgroundCond2:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).cond2 );

		case BackgroundDoor:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).door );

		case BackgroundEbN0:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).ebn0 );

		case BackgroundFire:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).fire );

		case BackgroundKsvn:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).ksvn );

		case BackgroundLogBer:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).logBer );

		case BackgroundLogPer:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).logPer );

		case BackgroundMoving:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).moving );

		case BackgroundPower:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).power );

		case BackgroundPowerRef:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).powerRef );

		case BackgroundReactive:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).reactive );

		case BackgroundShum:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).shum );

		case BackgroundTemperature:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).temperature );

		case BackgroundTotal:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).total );

		case BackgroundUPSLoad:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).upsload );

		case BackgroundUPSPower:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).upspower );

		case BackgroundUPSUin:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).upsuin );

		case BackgroundUPSUout:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).upsuout );

		case BackgroundZahDemod:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).zahdemod );

		case BackgroundZahSignal:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).zahsignal );

		case BackgroundZapUst:
			return m_hosts.at( row ).errorColor( m_hosts.at( row ).zapust );

		case Cond1:
			return m_hosts.at( row ).cond1.value;

		case Cond2:
			return m_hosts.at( row ).cond2.value;

		case Door:
			return m_hosts.at( row ).door.value;

		case EbN0:
			return m_hosts.at( row ).ebn0.value;

		case ErrorList: {
			QStringList list;
			foreach( const HostError & hostError, m_hosts.at( row ).errorList )
				list << hostError.toString();

			return list.join('\n');
		}

		case ErrorsHistory:
			return m_hosts.at( row ).errorsHistory();

		case Fire:
			return m_hosts.at( row ).fire.value;

		case HostId:
			return m_hosts.at( row ).hostId;

		case Ksvn:
			return m_hosts.at( row ).ksvn.value;

		case LogBer:
			return m_hosts.at( row ).logBer.value;

		case LogPer:
			return m_hosts.at( row ).logPer.value;

		case Moving:
			return m_hosts.at( row ).moving.value;

		case Name:
			return m_hosts.at( row ).name;

		case Ping:
			return m_hosts.at( row ).ping.value;

		case Power:
			return m_hosts.at( row ).power.value;

		case PowerRef:
			return m_hosts.at( row ).powerRef.value;

		case Reactive:
			return m_hosts.at( row ).reactive.value;

		case Shum:
			return m_hosts.at( row ).shum.value;

		case Temperature:
			return m_hosts.at( row ).temperature.value;

		case Total:
			return m_hosts.at( row ).total.value;

		case UPSLoad:
			return m_hosts.at( row ).upsload.value;

		case UPSPower:
			return m_hosts.at( row ).upspower.value;

		case UPSUin:
			return m_hosts.at( row ).upsuin.value;

		case UPSUout:
			return m_hosts.at( row ).upsuout.value;

		case ZahDemod:
			return m_hosts.at( row ).zahdemod.value;

		case ZahSignal:
			return m_hosts.at( row ).zahsignal.value;

		case ZapUst:
			return m_hosts.at( row ).zapust.value;


		default:
			return QVariant();
	}
}

QVariant
ModelHost::data( int row, const QString & roleName ) const
{
	if ( m_hosts.isEmpty() )
		return QVariant();

	while ( row >= m_hosts.count() )
		row -= m_hosts.count();

	while ( row < 0 )
		row += m_hosts.count();

	return data( createIndex( row, 0 ), roleNames().key( roleName.toLatin1() ) );
}

QHash< int, QByteArray >
ModelHost::roleNames() const	// override final
{
	QHash< int, QByteArray > roles = QAbstractListModel::roleNames();
	roles[ Active ] = "active";
	roles[ AgentPing ] = "agentping";
	roles[ Background ] = "bgcolor";
	roles[ BackgroundActive ] = "bgactive";
	roles[ BackgroundAgentPing ] = "bgagentping";
	roles[ BackgroundCond1 ] = "bgcond1";
	roles[ BackgroundCond2 ] = "bgcond2";
	roles[ BackgroundDoor ] = "bgdoor";
	roles[ BackgroundEbN0 ] = "bgebn0";
	roles[ BackgroundFire ] = "bgfire";
	roles[ BackgroundKsvn ] = "bgksvn";
	roles[ BackgroundLogBer ] = "bglogber";
	roles[ BackgroundLogPer ] = "bglogper";
	roles[ BackgroundMoving ] = "bgmoving";
	roles[ BackgroundPing ] = "bgping";
	roles[ BackgroundPower ] = "bgpower";
	roles[ BackgroundPowerRef ] = "bgpowerref";
	roles[ BackgroundReactive ] = "bgreactive";
	roles[ BackgroundShum ] = "bgshum";
	roles[ BackgroundTemperature ] = "bgtemperature";
	roles[ BackgroundTotal ] = "bgtotal";
	roles[ BackgroundUPSLoad ] = "bgupsload";
	roles[ BackgroundUPSPower ] = "bgupspower";
	roles[ BackgroundUPSUin ] = "bgupsuin";
	roles[ BackgroundUPSUout ] = "bgupsuout";
	roles[ BackgroundZahDemod ] = "bgzahdemod";
	roles[ BackgroundZahSignal ] = "bgzahsignal";
	roles[ BackgroundZapUst ] = "bgzapust";
	roles[ Cond1 ] = "cond1";
	roles[ Cond2 ] = "cond2";
	roles[ Door ] = "door";
	roles[ EbN0 ] = "ebn0";
	roles[ ErrorList ] = "errorList";
	roles[ ErrorsHistory ] = "errorshistory";
	roles[ Fire ] = "fire";
	roles[ LogBer ] = "logber";
	roles[ LogPer ] = "logper";
	roles[ Ksvn ] = "ksvn";
	roles[ HostId ] = "hostId";
	roles[ Moving ] = "moving";
	roles[ Name ] = "name";
	roles[ Ping ] = "ping";
	roles[ Power ] = "power";
	roles[ PowerRef ] = "powerref";
	roles[ Reactive ] = "reactive";
	roles[ Shum ] = "shum";
	roles[ Temperature ] = "temperature";
	roles[ Total ] = "total";
	roles[ UPSLoad ] = "upsload";
	roles[ UPSPower ] = "upspower";
	roles[ UPSUin ] = "upsuin";
	roles[ UPSUout ] = "upsuout";
	roles[ ZahDemod ] = "zahdemod";
	roles[ ZahSignal ] = "zahsignal";
	roles[ ZapUst ] = "zapust";

	return roles;
}

int
ModelHost::count() const
{
	return m_hosts.count();
}

Host &
ModelHost::host( int i )
{
	return m_hosts[ i ];
}

const Host &
ModelHost::host( int i ) const
{
	return m_hosts.at( i );
}

void
ModelHost::set( const QString & hostId, const QString & name, const ZbxValue & value )
{
	for ( int i = 0; i < m_hosts.count(); ++i )
		if ( m_hosts.at( i ) == hostId ) {
			if ( m_hosts[ i ].set( name, value ) ) {
			//m_hosts[ i ].set( name, value );
				const QModelIndex index = createIndex( i, 0 );
				emit dataChanged( index, index );
				break;
			}
		}

	//layoutChanged();
}

void
ModelHost::sortByName()
{
	qSort( m_hosts );
}

void
ModelHost::resetErrors()
{
	for ( int i = 0; i < m_hosts.count(); ++i )
		m_hosts[ i ].errorList.clear();
}

void
ModelHost::resetHistoryErrors()
{
	for ( int i = 0; i < m_hosts.count(); ++i )
		m_hosts[ i ].historyErrorList.clear();
}

void
ModelHost::addError( const QString & hostId, const QString & triggerId, const QString & priority,
		const QString & description, const QString & time )
{
	for ( int i = 0; i < m_hosts.count(); ++i )
		if ( m_hosts.at( i ) == hostId ) {
			m_hosts[ i ].errorList.append( HostError( triggerId, priority, description, time ) );

			const QModelIndex index = createIndex( i, 0 );
			emit dataChanged( index, index );

			break;
		}
}

QMultiMap< QString, HostError >
ModelHost::sortErrors()
{
	QMultiMap< QString, HostError > all_errors;

	for ( int i = 0; i < m_hosts.count(); ++i ) {
		qSort( m_hosts[ i ].errorList );
		foreach( const HostError & he, m_hosts.at( i ).errorList ) {
			all_errors.insert( m_hosts.at( i ).hostId, he );
		}
	}

	return all_errors;
}

void
ModelHost::sortHistoryErrors()
{
	for ( int i = 0; i < m_hosts.count(); ++i )
		qSort( m_hosts[ i ].historyErrorList );
}

void
ModelHost::addHistoryError( const QString & hostId, const QString & triggerId, const QString & priority,
		const QString & description, const QString & time )
{
	for ( int i = 0; i < m_hosts.count(); ++i ) {
		if ( m_hosts.at( i ) == hostId ) {
			m_hosts[ i ].historyErrorList.append( HostError( triggerId, priority, description, time ) );
			break;
		}
	}
}

QVariantList
ModelHost::hostIds() const
{
	QVariantList list_hosts;

	for ( int i = 0; i < m_hosts.count(); ++i )
		list_hosts << m_hosts.at( i ).hostId;

	return list_hosts;
}


/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#ifndef MODELERROR_H
#define MODELERROR_H

#include <QAbstractListModel>
#include <QList>

#include "HostError.h"

class ModelHost;

/** \brief
 */
class ModelError : public QAbstractListModel
{
	Q_OBJECT

		enum Roles {
			Text = Qt::UserRole + 1,
			HostId,
			Color
		};

	private:
		QList< HostError > m_errors;

	public:
		ModelError( QObject * parent );

		void update( const ModelHost * modelHost );

		int count() const;

		int rowCount( const QModelIndex & parent ) const override final;

		QVariant data( const QModelIndex & index, int role ) const override final;

		QHash< int, QByteArray > roleNames() const override final;

		const HostError & at( int index ) const;
};

#endif


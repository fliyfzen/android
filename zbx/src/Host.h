/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#ifndef HOST_H
#define HOST_H

#include <QList>
#include <QString>

#include <QGeoCoordinate>

#include "HostError.h"
#include "ZbxValue.h"

/** \brief 
 */
struct Host
{
	QString hostId,
			name;

	ZbxValue active,		// активная мощность
			 agentPing,
			 cond1,
			 cond2,
			 door,
			 ebn0,
			 fire,
			 ksvn,			///< КСВН
			 logBer,
			 logPer,
			 ping,
			 power,
			 powerRef,		// отражённая можность
			 reactive,		// реактивная мощность
			 shum,			// отношение несущая/шум
			 upsuin,
			 upsuout,
			 upspower,
			 upsload,
			 temperature,
			 total,			// счетик, значение от сброса
			 frequency,
			 moving,
			 zahdemod,		// захват демодулятора
			 zahsignal,		// захват сигнала
			 zapust;		// запас устойчивости связи

	QList< HostError > historyErrorList,
					   errorList;

	bool operator == ( const QString & other ) const;

	bool operator < ( const Host & other ) const;

	/** \brief Устанавливает новое значение
	 *
	 * \return Если значение изменилось, возвращает true.
	 */
	bool set( const QString & name, const ZbxValue & value );

	QString errorColor() const;

	/** \brief Цвет ошибки для \a value
	 *
	 * Ищет value.itemId в \a errorList.
	 */
	QString errorColor( const ZbxValue & value ) const;

	/** \brief Список ошибок в rich text разметке
	 */
	QString richErrors() const;

	QString errorsHistory() const;

	QGeoCoordinate coordinate() const;

	/** \brief Координаты по имени объекта
	 */
	static QGeoCoordinate coordinate( const QString & name );
};

#endif


/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#include "ZbxValue.h"

ZbxValue::ZbxValue()
	: value( ZBXV_UNKNOWN )
{
}

ZbxValue::ZbxValue( const char * nameForSearch, const char * nameForQml )
{
}

ZbxValue::ZbxValue( const QString & iId, const QString & val )
	: itemId( iId ), value( val )
{
}

ZbxValue &
ZbxValue::operator += ( const QString & str )
{
	value += str;

	return *this;
}


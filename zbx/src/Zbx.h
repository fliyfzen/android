/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#ifndef ZBX_H
#define ZBX_H

#include <QObject>

#include <QNetworkAccessManager>
//#include <QUrl>
#include <QVariant>

class QNetworkAccessManager;
class QNetworkReply;

/** \brief Получение данных из Zabbix по JSON-RPC
 */
class Zbx : public QObject
{
	Q_OBJECT

	public:
		enum Type : int {
			Version = 1,
			Login,
			HostGroup,
			Values,
			ErrorsHistory,		///< Все ошибки
			CurrentErrors		///< Текущие ошибки
		};

	private:
		QNetworkAccessManager * m_nam;


		void query( const QJsonObject & object );

		QString m_sessionKey;		// 32 chars

	private Q_SLOTS:
		void readReply( QNetworkReply * reply );
		void networkAccessibilityChanged( QNetworkAccessManager::NetworkAccessibility accessible );

	public:
		Zbx( QObject * parent = nullptr );

		void get( Type type, const QVariantList & params = QVariantList() );
		QNetworkAccessManager::NetworkAccessibility networkAccessible() const;

		bool isOpen() const;		///< m_sessionKey is not empty

	Q_SIGNALS:
		void dataReady( const QJsonObject & object );
		void loginOk();
};

#endif


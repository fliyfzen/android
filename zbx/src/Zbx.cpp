/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#include "Zbx.h"

#include <QtNetwork>

Zbx::Zbx( QObject * parent )
	: QObject( parent )
{
	m_nam = new QNetworkAccessManager( this );

	connect( m_nam, SIGNAL( finished( QNetworkReply * ) ), SLOT( readReply( QNetworkReply * ) ) );
}

void
Zbx::get( Type type, const QVariantList & params )
{
	if ( type != Login && m_sessionKey.isEmpty() ) {
		qDebug() << "No session key";
		return;
	}

	QJsonObject j_object;
	j_object.insert("jsonrpc", "2.0");
	j_object.insert("id", type );
	j_object.insert("auth", m_sessionKey.isEmpty() ? QJsonValue() : m_sessionKey );

	switch ( type ) {
		case Version: {
			j_object.insert("method", "apiinfo.version");
			j_object.insert("auth", QJsonValue() );		// null
			j_object.insert("params", QJsonObject() );	// empty
			return query( j_object );
		}

		case Login: {
			if ( params.count() < 2 )
				return;
			j_object.insert("method", "user.login");
			QJsonObject j_params;
			j_params.insert("user", params.at( 0 ).toString() );
			j_params.insert("password", params.at( 1 ).toString() );
			j_object.insert("params", j_params );
			return query( j_object );
		}

		case HostGroup: {
			j_object.insert("method", "host.get");
			QJsonObject j_params;
			QJsonArray j_output;
			j_output.append("hostid");
			j_params.insert("output", j_output );
			j_params.insert("selectGroups", "extend");
			QJsonArray j_groupid;
			j_groupid.append( REGION_CODE );
			j_params.insert("filter", j_groupid );
			j_object.insert("params", j_params );
			return query( j_object );
		}

		case Values: {
			j_object.insert("method", "item.get");
			QJsonObject j_params;
			j_params.insert("output", "extend");
			QJsonArray j_hosts;
			foreach( const QVariant & v, params )
				j_hosts.append( v.toString() );
			j_params.insert("hostids", j_hosts );
			j_params.insert("selectTriggers", true );
			j_object.insert("params", j_params );
			j_object.insert("sortfield", "name");
			return query( j_object );
		}

		case ErrorsHistory: {
			j_object.insert("method", "trigger.get");
			QJsonObject j_params;
			j_params.insert("output", "extend");
			j_params.insert("expandData", "hostname");
            j_params.insert("hostids", "10331");
			QJsonArray j_hosts;
			foreach( const QVariant & v, params )
				j_hosts.append( v.toString() );
			j_params.insert("hostids", j_hosts );
			j_object.insert("params", j_params );
			j_object.insert("sortfield", "lastchange");
			j_object.insert("sortorder", "DESC");
			return query( j_object );
		}

		case CurrentErrors: {
			j_object.insert("method", "trigger.get");
			QJsonObject j_params;
			j_params.insert("output", "extend");
			j_params.insert("selectHosts", "extend");
			j_params.insert("selectItems", "extend");
			j_params.insert("monitored", "true");
			j_params.insert("min_severity", "2");
			QJsonObject j_filter;
			j_filter.insert("value", 1 );
			j_filter.insert("priority", QJsonArray() << 2 << 3 << 4 << 5 );
			QJsonArray j_hosts;
			foreach( const QVariant & v, params )
				j_hosts.append( v.toString() );
            j_params.insert("hostid", j_hosts );
			j_params.insert("sortfield", QJsonArray() << "lastchange" << "priority" << "hostname");
			j_params.insert("sortorder", "DESC");
			j_params.insert("selectLastEvent", "extend");
			j_params.insert("filter", j_filter );
			j_object.insert("params", j_params );
			return query( j_object );
		}

		default:
			;
    }
}

void
Zbx::query( const QJsonObject & object )
{
	QSettings s;
	QString s_url = s.value("url").toString();
	if ( ! s_url.startsWith( QLatin1String("http") ) )
		s_url.prepend( QLatin1String("http://") );

    QUrl url( s_url + "/api_jsonrpc.php");
	if ( ! url.isValid() ) {
		qDebug() << tr("ERROR: URL invalid: %1").arg( url.toDisplayString() );
		return;
	}

    QNetworkRequest request( url );
	request.setRawHeader("Content-Type", "application/json-rpc");
    m_nam->post( request, QJsonDocument( object ).toJson( QJsonDocument::Compact ) );
}

void
Zbx::readReply( QNetworkReply * reply )		// private slot
{
	const QByteArray data = reply->readAll();

	//qDebug() << "answer" << QString( QJsonDocument::fromJson( data ).toJson() );
	//
	// TODO обработать ошибки

	const QJsonObject object = QJsonDocument::fromJson( data ).object();
	if ( object.isEmpty() ) {
		qDebug() << "No data received.";
		return;
	}

	if ( ! object.contains("id") ) {
		qDebug() << "No ID in data.";
		return;
	}

	if ( object.contains("error") ) {
		const QJsonObject j_error = object.value("error").toObject();
		qDebug() << QString("ERROR: %1 - %2, %3")
			.arg( j_error.value("code").toString() )
			.arg( j_error.value("message").toString() )
			.arg( j_error.value("data").toString() );
		return;
	}

	if ( object.value("id").toInt() == Login	) {
		m_sessionKey = object.value("result").toString();
        qDebug() << m_sessionKey;
		if ( ! m_sessionKey.isEmpty() )
			emit loginOk();

	} else
		emit dataReady( object );
}

QNetworkAccessManager::NetworkAccessibility
Zbx::networkAccessible() const
{
	return m_nam->networkAccessible();
}

bool
Zbx::isOpen() const		///< m_sessionKey is not empty
{
	return ! m_sessionKey.isEmpty();
}

void
Zbx::networkAccessibilityChanged( QNetworkAccessManager::NetworkAccessibility accessible )
{
	if ( accessible != QNetworkAccessManager::Accessible ) {
		m_sessionKey.clear();
	}
}


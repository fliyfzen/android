/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#include "Host.h"

#include <QtPositioning>

#include <QDebug>

// Zabbix Item
#define ZI_COND_1 "0. Конд. 1"		// Кондиционер 1
#define ZI_COND_2 "0. Конд. 2"		// Кондиционер 2
#define ZI_POWER "10.Harris Выходная мощность"		// мощность
#define ZI_POWER_REF "12.Harris Отраженная мощность"	// мощность отражённая
#define ZI_TEMPERATURE "0. Температура"	// температура
#define ZI_FREQUENCY "Частота"	// частота
#define ZI_PING "ping (sec)"	// пинг
#define ZI_AGENT_PING "Agent ping"	// Agent ping
#define ZI_DOOR "2.Входная Дверь"
#define ZI_MOVING "3.Датчик движения"	// Датчик движения
#define ZI_FIRE "1.Пожар"			// Пожар
#define ZI_KSVN "11.Harris КСВН"	// КСВН
#define ZI_EBN0 "13.Harmonic Eb/N0"
#define ZI_LOGBER "13.Harmonic log(BER)"
#define ZI_LOGPER "13.Harmonic log(PER)"
#define ZI_ZAPUST "13.Harmonic Запас устойчивости связи"	// Запас устойчивости связи
#define ZI_ZAHDEMOD "13.Harmonic Захват демодулятора"		// Захват демодулятора
#define ZI_ZAHSIGNAL "13.Harmonic Захват сигнала"			// Захват сигнала
#define ZI_SHUM "13.Harmonic Отношение несущая/шум (CNR)"
#define ZI_UPSUIN "6.ИБП Uв"					// Вход В.
#define ZI_UPSUOUT "7. ИБП Uo"					// Выход В.
#define ZI_UPSPOWER "8. ИБП Pw"					// Мощность в Вт.
#define ZI_UPSLOAD "9. ИБП Load"				// Загрузка в %
#define ZI_TOTAL "Расход электроэнергии от сброса"	//
#define ZI_ACTIV "Активная мощность"
#define ZI_REACTIV "Реактивная мощность"

bool
Host::operator == ( const QString & other ) const
{
	return hostId == other;
}

bool
Host::operator < ( const Host & other ) const
{
	return name < other.name;
}

bool
Host::set( const QString & name, const ZbxValue & value )
{
	auto setValue = []( ZbxValue & v_old, const ZbxValue & v_new, const QString & suffix = QString() )->bool
	{
		const QString old_value = v_old.value;
		v_old = v_new;

		if ( ! suffix.isEmpty() )
			v_old += suffix;

		return old_value != v_new.value;
	};

	/*
	auto setString = []( QString & s_old, const QString & s_new )->bool
	{
		const bool rc = s_old != s_new;
		s_old = s_new;
		return rc;
	};
	*/
	auto prettyPower = []( const ZbxValue & value )->ZbxValue
	{
		bool ok;
		qreal n = value.value.toDouble( &ok );
		QString ei = QObject::tr("W");
		if ( n > 1000. ) {
			n /= 1000.;
			ei = QObject::tr("kW");
		}

		return ZbxValue( value.itemId, ok ? QString("%1 %2").arg( n, 0, 'f', 2 ).arg( ei ) : ZBXV_UNKNOWN );
	};

	// Округлять до сотых и добавить единици измерения
	auto decibel = []( const ZbxValue & value )->ZbxValue
	{
		const QString p_str = value.value;
		bool ok;
		qreal n = value.value.toDouble( &ok );
		return ZbxValue( value.itemId, ok ? QObject::tr("%1 dB").arg( n, 0, 'f', 1 ) : ZBXV_UNKNOWN );
	};
	// кондиционер
	auto condition = []( const ZbxValue & value )->ZbxValue
	{
		QString p_str = value.value;
		if ( p_str == "1" || p_str == "3" )
			p_str = "OK";
		else if ( p_str == "2" || p_str == "8" )
			p_str = "Ожидание";
		else if ( p_str == "4" || p_str == "6" || p_str == "7" )
			p_str = "Ошибка";
		else if ( p_str == "10" || p_str == "11" )
			p_str = "Нет связи";

		return ZbxValue( value.itemId, p_str );
	};

	if ( name == ZI_POWER )
		return setValue( power, prettyPower( value ) );

	if ( name == ZI_POWER_REF )
		return setValue( powerRef, prettyPower( value ) );

	if ( name == ZI_TEMPERATURE ) {
		ZbxValue new_temp = value;
		new_temp.value +=  QObject::tr(" °C");
		return setValue( temperature, new_temp );
	}

	if ( name == ZI_FREQUENCY )
		return setValue( frequency, value );

	if ( name == ZI_PING ) {
		const int n = qRound( value.value.toDouble() );
		ZbxValue v = value;
		v.value = QString("%1 ms.").arg( n );
		//ping = QString("%1 ms.").arg( n );
		return setValue( ping, v );
	}

	if ( name == ZI_AGENT_PING )
		return setValue( agentPing, value );

	if ( name == ZI_COND_1 )
		return setValue( cond1, condition( value ) );

	if ( name == ZI_COND_2 )
		return setValue( cond2, condition( value ) );

	if ( name == ZI_DOOR )
		return setValue( door, value );

	if ( name == ZI_MOVING )
		return setValue( moving, value );

	if ( name == ZI_FIRE )
		return setValue( fire, value );

	if ( name == ZI_KSVN )
		return setValue( ksvn, value );

	if ( name == ZI_EBN0 )
		return setValue( ebn0, decibel( value ) );

	if ( name == ZI_LOGBER )
		return setValue( logBer, value );

	if ( name == ZI_LOGPER )
		return setValue( logPer, value );

	if ( name == ZI_ZAPUST )
		return setValue( zapust, decibel( value ) );

	if ( name == ZI_ZAHDEMOD )
		return setValue( zahdemod, value );

	if ( name == ZI_ZAHSIGNAL )
		return setValue( zahsignal, value );

	if ( name == ZI_SHUM )
		return setValue( shum, decibel( value ) );

	if ( name == ZI_UPSUIN )
		return setValue( upsuin, value, QObject::tr(" V") );

	if ( name == ZI_UPSUOUT )
		return setValue( upsuout, value, QObject::tr(" V") );

	if ( name == ZI_UPSPOWER )
		return setValue( upspower, value, QObject::tr(" Wt") );

	if ( name == ZI_UPSLOAD )
		return setValue( upsload, value, "%");

	if ( name == ZI_TOTAL ) {
		bool ok;
		const qreal n = value.value.toDouble( &ok );
		ZbxValue v = ZbxValue( value.itemId, QObject::tr("%1 kWt").arg( n, 0, 'f', 2 ) );
		return setValue( total, v );
	}

	if ( name == ZI_ACTIV )
		return setValue( active, prettyPower( value ) );

	if ( name == ZI_REACTIV )
		return setValue( reactive, prettyPower( value ) );

	return true;
}

QString
Host::errorColor() const
{
	if ( errorList.isEmpty() )
		return HostError::color();

	int priority = 0;

	foreach( const HostError & herror, errorList )
		if ( ( priority = herror.priority ) == 5 )
			break;

	return HostError::color( priority );
}

QString
Host::errorColor( const ZbxValue & value ) const
{
	if ( value.itemId.isEmpty() )
		return HostError::color();

	foreach( const HostError & hostError, errorList )
		if ( hostError.itemId == value.itemId )
			return HostError::color( hostError.priority );

	return HostError::color();
}

QString
Host::richErrors() const
{
	QString str;

	for ( int i = 0; i < errorList.count(); ++i ) {
		str += errorList.at( i ).toString();
		str += "<br>";
	}

	return str;
}


QString
Host::errorsHistory() const
{
	QString str = "<table width=100% cellspacing=5>";

	int rowNum = 0;		// для чересстрочного раскрашивания фона строк

	for ( int i = 0; i < historyErrorList.count(); ++i ) {
		str += historyErrorList.at( i ).toHtmlTableRow( ++rowNum );
		//++rowNum;
	}

	return str;
}

QGeoCoordinate
Host::coordinate() const
{
	return coordinate( name );
}

QGeoCoordinate
Host::coordinate( const QString & name )		// static
{
	static QHash< QString, QGeoCoordinate > data = {
		{"ТУЛА", QGeoCoordinate( 54.14171, 37.58443 ) },
		{"БЕЛЕВ", QGeoCoordinate( 53.813, 36.133 ) },
		{"МАРЬИНСКИЙ", QGeoCoordinate( 53.596, 36.124 ) },
		{"АРСЕНЬЕВО", QGeoCoordinate( 53.737, 36.68299 ) },
		{"СЛАВНЫЙ", QGeoCoordinate( 53.54, 36.498 ) },
		{"ЧЕРНЬ", QGeoCoordinate( 53.454, 36.92499 ) },
		{"ЛИПИЦЫ", QGeoCoordinate( 53.356, 37.273 ) },
		{"АРХАНГЕЛЬСКОЕ", QGeoCoordinate( 53.254, 37.71 ) },
		{"КРАПИВНА", QGeoCoordinate( 53.936, 37.12699 ) },
		{"ЗАОКСКИЙ", QGeoCoordinate( 54.724, 37.392 ) },
		{"ДУБНА", QGeoCoordinate( 54.129, 36.99299 ) },
		{"ПЛАВСК", QGeoCoordinate( 53.696, 37.278 ) },
		{"ТЕПЛОЕ", QGeoCoordinate( 53.604, 37.585 ) },
		{"ЕФРЕМОВ", QGeoCoordinate( 53.13354, 38.10362 ) },
		{"ВОЛОВО", QGeoCoordinate( 53.565, 38.00999 ) },
		{"НОВОМОСКОВСК", QGeoCoordinate( 54.04321, 38.24894 ) },
		{"КУРКИНО", QGeoCoordinate( 53.433, 38.667 ) },
		{"ЖУРИШКИ", QGeoCoordinate( 53.697, 38.72699 ) },
		{"МОРДВЕС", QGeoCoordinate( 54.568, 38.31799 ) },
		{"ХАТМАНОВО", QGeoCoordinate( 54.56, 38.72699 ) },
		{"БАШИНО", QGeoCoordinate( 54.703, 37.93299 ) },
		{"ОДОЕВ", QGeoCoordinate( 53.945, 36.63699 ) },
		{"АЛЕКСИН", QGeoCoordinate( 54.506, 37.115 ) },
		{"СУВОРОВ", QGeoCoordinate( 54.121, 36.50099 ) }
	};

	return data.value( name, QGeoCoordinate( 54.14171, 37.58443 ) );	// по-умолчанию Тула
}


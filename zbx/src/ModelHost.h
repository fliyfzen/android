/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#ifndef MODELHOST_H
#define MODELHOST_H

#include <QAbstractListModel>

#include "Host.h"

class Zbx;

/** \brief 
 */
class ModelHost : public QAbstractListModel
{
	Q_OBJECT

	public:
		enum Roles {
			Active = Qt::UserRole + 1,			///< Активная мощность
			AgentPing,
			Background,
			BackgroundActive,
			BackgroundAgentPing,
			BackgroundCond1,
			BackgroundCond2,
			BackgroundDoor,
			BackgroundEbN0,
			BackgroundFire,
			BackgroundKsvn,
			BackgroundLogBer,
			BackgroundLogPer,
			BackgroundMoving,
			BackgroundPing,
			BackgroundPower,
			BackgroundPowerRef,
			BackgroundReactive,
			BackgroundShum,
			BackgroundUPSLoad,
			BackgroundUPSPower,
			BackgroundUPSUin,
			BackgroundUPSUout,
			BackgroundTemperature,
			BackgroundTotal,
			BackgroundZahDemod,
			BackgroundZahSignal,
			BackgroundZapUst,
			Cond1,		///< Кондиционер 1
			Cond2,		///< Кондиционер 2
			Door,		///< Входная дверь
			EbN0,
			ErrorsHistory,		///< История ошибок
			ErrorList,
			Fire,		///< Пожар
			HostId,
			Ksvn,		///< KSVN
			LogBer,
			LogPer,
			Moving,		///< Датчик движения
			Name,
			Ping,
			Power,
			PowerRef,
			Reactive,	///< Реактивная мощность
			Shum,		///< Отношение нусущая/шум
			Temperature,
			Total,		///< Счётчик, значение от сброса
			UPSLoad,
			UPSPower,
			UPSUin,		///< UPS U in
			UPSUout,	///< UPS U out
			ZahDemod,	///< Захват демодулятора
			ZahSignal,	///< Захват сигнала
			ZapUst		///< Запас устойчивости связи
		};

	private:
		QList< Host > m_hosts;

	public:
		explicit ModelHost( QObject * parent );

		void append( const Host & host );

		int rowCount( const QModelIndex & parent ) const override final;

		QVariant data( const QModelIndex & index, int role ) const override final;

		QVariant data( int row, const QString & roleName ) const;

		QHash< int, QByteArray > roleNames() const override final;

		int count() const;

		Host & host( int i );

		const Host & host( int i ) const;

		/** \brief Устанавливает значение \a value для параметра с именем \a name
		 */
		void set( const QString & hostId, const QString & name, const ZbxValue & value );

		void sortByName();

		QMultiMap< QString, HostError > sortErrors();

		void sortHistoryErrors();

		/** \brief Удаляет все ошибки в хостах
		 */
		void resetErrors();

		/** \brief Удаляет все ошибки из истории
		 */
		void resetHistoryErrors();

		/** \brief Добавляет ошивку с список ошибок хоста
		 */
		void addError( const QString & hostId, const QString & triggerId, const QString & priority,
				const QString & description, const QString & time );

		/** \brief Добавляет ошивку с список ошибок хоста
		 */
		void addHistoryError( const QString & hostId, const QString & triggerId, const QString & priority,
				const QString & description, const QString & time );

		QVariantList hostIds() const;
};

#endif


/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#include "ModelError.h"

#include "ModelHost.h"

ModelError::ModelError( QObject * parent )
	: QAbstractListModel( parent )
{
}

void
ModelError::update( const ModelHost * modelHost )
{
	m_errors.clear();
	layoutChanged();

	for ( int i = 0; i < modelHost->count(); ++i ) {
		if ( ! modelHost->host( i ).errorList.isEmpty() )
			foreach( const HostError & he, modelHost->host( i ).errorList ) {
				HostError hen = he;
                // Дополнить ошибку информацией о хосте
				hen.hostId = modelHost->host( i ).hostId;
				hen.hostName = modelHost->host( i ).name;
				m_errors << hen;
			}
	}

	layoutChanged();
}

int
ModelError::count() const
{
	return m_errors.count();
}

int
ModelError::rowCount( const QModelIndex & parent ) const		// override final
{
	if ( parent.isValid() )
		return 0;

	return m_errors.count();
}

QVariant
ModelError::data( const QModelIndex & index, int role ) const	// override final
{
	if ( ! index.isValid() )
		return QVariant();

	const int row = index.row();

	switch ( role ) {

		case Text:
			return QString("%1. %2: %3")
				.arg( m_errors.at( row ).dateTime.toString("dd.MM.yy hh:mm:ss") )
				.arg( m_errors.at( row ).hostName )
				.arg( m_errors.at( row ).description );

		case HostId:
			return m_errors.at( row ).hostId;

		case Color:
			return HostError::color( m_errors.at( row ).priority );

		default:
			return QVariant();
	}
}

QHash< int, QByteArray >
ModelError::roleNames() const									// override final;
{
	QHash< int, QByteArray > roles = QAbstractListModel::roleNames();
	roles[ Text ] = "text";
	roles[ HostId ] = "hostId";
	roles[ Color ] = "color";

	return roles;
}

const HostError &
ModelError::at( int index ) const
{
	return m_errors.at( index );
}


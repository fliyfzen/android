/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#ifndef HOSTERROR_H
#define HOSTERROR_H

#include <QDateTime>
#include <QString>

/** \brief 
 */
struct HostError
{
	QString hostId;

	QString itemId;

	uint priority;

	QString hostName;

	QString description;

	QDateTime dateTime;

	HostError( const QString & iId, const QString & prior, const QString & descr, const QString & time );

	bool operator < ( const HostError & other ) const;

	QString toString() const;

	QString toHtmlTableRow( int rowNum ) const;

	/** \brief Цвета ошибок
	 *
	 * https://www.w3.org/TR/SVG/types.html#ColorKeywords
	 */
	static QString color( int priority = 0 );
};


#endif


/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#ifndef ZBXVALUE_H
#define ZBXVALUE_H

#include <QString>

#define ZBXV_UNKNOWN "???"

/** \brief Значение с itemId
 */
//template< const char * nameForSearh, const nameForQml >
class ZbxValue
{
	public:
		QString itemId;

		QString value;

	public:
		ZbxValue();

		ZbxValue( const char * nameForSearch, const char * nameForQml );

		ZbxValue( const QString & iId, const QString & val );

		ZbxValue & operator += ( const QString & str );
};

#endif


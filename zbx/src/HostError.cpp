/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#include "HostError.h"

HostError::HostError( const QString & iId, const QString & prior, const QString & descr, const QString & time )
	: itemId( iId ), priority( prior.toUInt() ), description( descr )
{
	if ( ! time.isEmpty() && time != QLatin1String("0") )
		dateTime.setTime_t( time.toUInt() );
}

bool
HostError::operator < ( const HostError & other ) const
{
	return dateTime > other.dateTime;	// сортировать в обратном порядке
}

QString
HostError::toString() const
{
	return QString("<b>%1</b>: %2 <span bgcolor=%3><font color=black>%4</font></span>")
		.arg( priority )
		.arg( dateTime.isValid() ? dateTime.toString("dd.MM.yy_hh:mm:ss") : QString( 17, '-') )
		.arg( color( priority ) )
		.arg( description );
}

QString
HostError::color( int priority )
{
	switch ( priority ) {
		case 1:
			return "#fff6a5";

		case 2:
			return "#fff6a5";

		case 3:
			return "#ffb689";

		case 4:
			return "#ff9999";

		case 5:
			return "#ff3838";

		default:
			return "#aaffaa";
	}
}

QString
HostError::toHtmlTableRow( int rowNum ) const
{
	//return QString("<b>%1</b>: %2 <span bgcolor=%3><font color=black>%4</font></span>")
	return QString("<tr>"
			//"<td bgcolor=%1>%2:</td>"
			"<td bgcolor=%1>%2</td>"
			"<td bgcolor=%1>%3</td>"
		"</tr>")
		.arg( color( priority ) )
		//.arg( priority )
		.arg( dateTime.isValid() ? dateTime.toString("dd.MM.yy_hh:mm:ss") : QString( 17, '-') )
		.arg( description );
}


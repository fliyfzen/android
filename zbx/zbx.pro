################################################################################
#               __________                               __________            #
#               ___  ____/___________       ___   __________  /_  /_           #
#               __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           #
#               _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             #
#               /_____/  \___/ \____/       _____/ \____//_/  \__/             #
#                                                                              #
#     © 2016                                                                   #
#                                                                              #
################################################################################

TEMPLATE = lib
TARGET = zbx
INCLUDEPATH += . src
QT += network positioning
#QMAKE_CXXFLAGS += -mmmx -msse2 -msse3 -msse4.1 -m3dnow -std=gnu++11

HEADERS += src/Host.h \
		   src/HostError.h \
		   src/ModelError.h \
		   src/ModelHost.h \
		   src/Zbx.h \
		   src/ZbxValue.h

SOURCES += src/Host.cpp \
		   src/HostError.cpp \
		   src/ModelError.cpp \
		   src/ModelHost.cpp \
		   src/Zbx.cpp \
		   src/ZbxValue.cpp

RESOURCES += translations.qrc

LANGUAGES = ru en
# Create .ts files
defineReplace( prependAll ) {
	for( a, $$1 ):result += $$2$${a}$$3
	return( $$result )
}


tsroot = $$join( TARGET,,,.ts )
tstarget = $$join( TARGET,,,_ )
TRANSLATIONS = $$PWD/translations/$$tsroot
TRANSLATIONS += $$prependAll( LANGUAGES, $$PWD/translations/$$tstarget, .ts )

qtPrepareTool( LRELEASE, lrelease )
for( tsfile, TRANSLATIONS ) {
	command = $$LRELEASE $$tsfile
	system( $$command ) | error("Failed to run: $$command")
}

include(../customer.pri)

#RESOURCES += qrc/images.qrc \

#TRANSLATIONS = translations/mcc_ru.ts

#QMAKE_POST_LINK = lupdate $${_PRO_FILE_}		# обновить переводы


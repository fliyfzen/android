<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ButtonMoreLess</name>
    <message>
        <source>less</source>
        <translation>меньше</translation>
    </message>
    <message>
        <source>more</source>
        <translation>больше</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Door</source>
        <translation>Вх.дверь</translation>
    </message>
    <message>
        <source>Moving</source>
        <translation>Движение</translation>
    </message>
    <message>
        <source>Fire</source>
        <translation>Пожар</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Температура</translation>
    </message>
    <message>
        <source>Cond 1</source>
        <translation>Конд.1</translation>
    </message>
    <message>
        <source>Cond 2</source>
        <translation>Конд.2</translation>
    </message>
    <message>
        <source>Receiver</source>
        <translation>Приёмник</translation>
    </message>
    <message>
        <source>Output power</source>
        <translation>Вых.мощ</translation>
    </message>
    <message>
        <source>КСВН</source>
        <translation></translation>
    </message>
    <message>
        <source>Отраж. мощность</source>
        <translation>Отр.мощность</translation>
    </message>
    <message>
        <source>Transmitter</source>
        <translation>Передатчик</translation>
    </message>
    <message>
        <source>Eb/N0</source>
        <translation></translation>
    </message>
    <message>
        <source>log(BER)</source>
        <translation></translation>
    </message>
    <message>
        <source>log(PER)</source>
        <translation></translation>
    </message>
    <message>
        <source>Запас устойчивости связи</source>
        <translation>Запас уст. связи</translation>
    </message>
    <message>
        <source>Захват демодулятора</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Захват сигнала</source>
        <translation>Захв. сигнала</translation>
    </message>
    <message>
        <source>Отношение несущая/шум</source>
        <translation>Отн. несущая/шум</translation>
    </message>
    <message>
        <source>UPS</source>
        <translation>ИБП</translation>
    </message>
    <message>
        <source>U in</source>
        <translation>U вход</translation>
    </message>
    <message>
        <source>U out</source>
        <translation>U выход</translation>
    </message>
    <message>
        <source>Нагрузка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Загрузка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counter</source>
        <translation>Счётчик</translation>
    </message>
    <message>
        <source>Показания</source>
        <translation>Показ.</translation>
    </message>
    <message>
        <source>Params</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <source>Errors history</source>
        <translation>История ошибок</translation>
    </message>
    <message>
        <source>Charts</source>
        <translation>Графики</translation>
    </message>
    <message>
        <source>Нагрузка А</source>
        <translation>Нагрузка</translation>
    </message>
    <message>
        <source>Нагрузка Р</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DSP</source>
        <translation></translation>
    </message>
    <message>
        <source>TS</source>
        <translation></translation>
    </message>
    <message>
        <source>TX state</source>
        <translation>TX статус</translation>
    </message>
    <message>
        <source>DUC state</source>
        <translation>DUC статус</translation>
    </message>
    <message>
        <source>DUC temp</source>
        <translation>DUC темп.</translation>
    </message>
    <message>
        <source>Mod state</source>
        <translation>Mod статус</translation>
    </message>
    <message>
        <source>Mod temp</source>
        <translation>Mod темп.</translation>
    </message>
</context>
<context>
    <name>QmlApplication</name>
    <message>
        <source>updated at %1</source>
        <translation>обновлено %1</translation>
    </message>
    <message>
        <source>No error for index %1</source>
        <translation>Нет ошибок по индексу %1</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;br&gt;Host ID: %2&lt;br&gt;Power: %3&lt;br&gt;Temperature: %4&lt;br&gt;Errors:&lt;br&gt;%5&lt;br&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;br&gt;Хост ID: %2&lt;br&gt;Мощность: %3&lt;br&gt;Температура: %4&lt;br&gt;Ошибки:&lt;br&gt;%5&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;font color=red&gt;Host with ID=%1 not found.&lt;/font&gt;</source>
        <translation>&lt;font color=red&gt;Хост с ID=%1 не найден.&lt;/font&gt;</translation>
    </message>
    <message>
        <source>skiped at %1</source>
        <translation>пропущено в %1</translation>
    </message>
    <message>
        <source>Power: %1
Temperature: %2</source>
        <translation>Мощность: %1
Температура: %2</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Client</source>
        <translation>Клиент</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Карта</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation>Настройки...</translation>
    </message>
    <message>
        <source>Messages...</source>
        <translation>Сообщения...</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Wt, </source>
        <translation type="vanished">Вт, </translation>
    </message>
    <message>
        <source>Host ID: </source>
        <translation type="vanished">ID хоста:</translation>
    </message>
    <message>
        <source>Name: </source>
        <translation type="vanished">Название:</translation>
    </message>
    <message>
        <source>Power: </source>
        <translation type="vanished">Мощность:</translation>
    </message>
    <message>
        <source>Temperature: </source>
        <translation type="vanished">Температура:</translation>
    </message>
    <message>
        <source>Errors:
</source>
        <translation type="vanished">Ошибки:</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>type URL here</source>
        <translation>введите URL здесь</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <source>username for connect</source>
        <translation type="vanished">имя пользователя для соединения</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Interval</source>
        <translation>Интервал</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>Errors:</source>
        <translation type="vanished">Ошибки:</translation>
    </message>
    <message>
        <source>Wellcome. Please wait...</source>
        <translation type="vanished">Добро пожаловать. Пожалуйста подождите...</translation>
    </message>
    <message>
        <source>Please wait...</source>
        <translation>Пожалуйста подождите...</translation>
    </message>
    <message>
        <source>Station list</source>
        <translation>Список станций</translation>
    </message>
</context>
</TS>

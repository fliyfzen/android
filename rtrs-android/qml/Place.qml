/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

import QtQuick 2.7
import QtPositioning 5.5
import QtLocation 5.6
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

MapQuickItem {
	id: place

	property int modelIndex		///< Соответсвующий индекс в модели
	property string text
	property string color
	property string dropText

	signal clicked( int modelIndex )

	sourceItem: Rectangle {
		color: "transparent"
	    width: Math.max( itemText.implicitWidth, baloon.implicitWidth )
	    height: itemText.implicitHeight + baloon.height

		ColumnLayout {
			Baloon {
				id: baloon
				//x: place.width / 2 - 10
				//y: -height

				//width: 54
				//height: 54
				color: place.color
				text: place.dropText

				onClicked: place.clicked( place.modelIndex )
			}

			Text {
				id: itemText
				text: place.text
				//padding: 5
			}
		}

   }

   anchorPoint.x: 10
   anchorPoint.y: baloon.height

	/*
	ToolTip{
		id: toolTip
		visible: false
		text: place.toolTip
	}
	*/

   /*
	Drop {
		x: place.width / 2 - width / 2
		y: -height
		color: place.color
		text: place.dropText

		MouseArea {
			anchors.fill: parent
			onClicked: place.clicked( place.modelIndex );
		}

		onClicked: place.clicked( place.modelIndex )
	}
	*/
	MouseArea{
		//x: Math.min( sourceItem.x, baloon.x )
		//y: Math.min( sourceItem.y, baloon.y )

		anchors.fill: parent
		//width: ( ( sourceItem.x + sourceItem.width ) > ( baloon.x + baloon.width ) ? ( sourceItem.x + sourceItem.width ) : ( baloon.x + baloon.width ) ) - x
			//height: ( ( sourceItem.y + sourceItem.height ) > ( baloon.y + baloon.height ) ? ( sourceItem.y + sourceItem.height ) : ( baloon.y + baloon.height ) ) - y

			//hoverEnabled: true
			//onEntered: toolTip.visible = true
			//onExited: toolTip.visible = false

		onClicked: {
				//console.log( "MA " + width + " " + height )
			place.clicked( modelIndex )
		}
	}
}


/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

import QtQuick 2.7
import QtPositioning 5.5
import QtLocation 5.6
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Item {
	id: item
	width: mrect.implicitWidth
	height: mrect.height + 20	// 20 на уголок

	property string text: "text"
	property string color: "transparent"

	signal clicked()

	Rectangle {
		id: mrect
		radius: 10
		width: innerText.implicitWidth
		height: innerText.implicitHeight
		color: item.color
		border.width: 1
		border.color: "gray"

		MouseArea {
			anchors.fill: parent
			onClicked: {
				console.log( "Baloon " + width + " " + height );
				item.clicked()
			}
		}

		Text {
			id: innerText
			text: item.text
			font.pixelSize: 9
			padding: 7
		}
	}

	//// Уголок
	Rectangle {
		id: angle
		color: item.color
		height: 25
		width: 15
		x: mrect.radius
		y: mrect.height - 5
		visible: false
	}

	Image {
		id: mask
		source: "baloon_angle_mask.png"
		x: mrect.radius
		y: mrect.height - 5
		visible: false
	}

	OpacityMask {
		//anchors.fill: parent
		height: 25
		width: 15
		x: mrect.radius
		y: mrect.height - 5
		source: angle
		maskSource: mask
	}

	Image {
		source: "baloon_angle_outline.png"
		x: mrect.radius
		y: mrect.height
	}
	///  уголок
}


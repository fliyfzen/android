/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtPositioning 5.5
import QtLocation 5.6
import QtGraphicalEffects 1.0

import online.ecovolt.qmlapplication 1.1

ApplicationWindow {
    id: window
    visible: true
    width: 480
    height: 640
    title: qsTr("Client")

	function addMessage( msg ) {
		messagesText.text = ( messagesText.text + msg + '\n' )
		//return "ok"
	}

	function busyShow( show ) {
		busy.visible = show
	}

	function setStatusText( msg ) {
		footer.text = msg
	}

	function addMapItem( index, name, coords ) {
		var mapItem = Qt.createQmlObject("Place {}", window );
		mapItem.modelIndex = index
		mapItem.text = name
		mapItem.coordinate = coords
		mapItem.clicked.connect( detailsActivate )

		if ( mapItem )
			map.addMapItem( mapItem )
	}

	function updateMapItem( name, color, dropText ) {
		map.updateMapItem( name, color, dropText )
	}

	function detailsActivate( index ) {
		detailsPage.refresh( index )
		detailsPage.setTabIndex( 0 )
		swipe.currentIndex = 1
	}

	function refreshDetails() {
		if ( swipe.currentIndex == 1 )
			detailsPage.refresh()
	}

    Settings {
        id: settings
        property string url: "ecovolt.online:8000"
		property string username: "admin"
		property string password: "yuzf7p5"
        property int interval: 30
    }

    header: ToolBar {
		id: toolBar

        RowLayout {
            spacing: 10
            anchors.fill: parent
            ToolButton {
                contentItem: Image {
                    source: "qrc:/gerb.png"
                }

                onClicked: optionsMenu.open()

                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight

                    MenuItem {
                        text: qsTr("Map")
                        onTriggered: swipe.currentIndex = 2
                    }
                    MenuItem {
                        text: qsTr("Station list")
                        onTriggered: swipe.currentIndex = 0
                    }
                    MenuItem {
                        text: qsTr("Settings...")
                        onTriggered: settingsPopup.open()
                    }
					MenuItem {
						text: qsTr("Messages...")
						onTriggered: messages.open()
					}
                    MenuItem {
                        text: qsTr("About")
                        onTriggered: aboutDialog.open()
                    }
                }
            }

			/*
			Label {
				id: errorCount
				text: errorComboBox.count
			}
			*/

			ComboBox {
				id: errorComboBox
				model: modelError
				textRole: "text"
				Layout.fillWidth: true

				property string color: "#cccccc"

				//delegate: ItemDelegate {
				delegate: ItemDelegate {

					width: errorComboBox.width
					height: 50
					Text {
						text: model.text
						anchors.fill: parent
						verticalAlignment: Text.AlignVCenter
						horizontalAlignment: Text.AlignLeft
						leftPadding: 10
						//color: model.color
						color: "black"
					}
					background: Rectangle {
						id: delegateBg
						anchors.fill: parent
						color: model.color
					}
				}

				contentItem: //Rectangle {
					//anchors.fill: parent
					//color: errorComboBox.delegate.background.color
					///color: errorComboBox.currentIndex != -1 ? errorComboBox.model.color : "gray"
					Text {
						anchors.fill: parent
						text: errorComboBox.currentIndex != -1 ? ( errorComboBox.displayText ) : ("Ошибок: " + errorComboBox.count )
						verticalAlignment: Text.AlignVCenter
						horizontalAlignment: Text.AlignLeft
					}
				//}

				onActivated: {
					var index = QmlApplication.getModelIndexByErrorIndex( currentIndex );

					if ( index != -1 )
						detailsActivate( index )
				}
			}
        }
    }

	footer: Rectangle {
		id: statusBar
		height: 20
		width: parent.width

		Text {
			id: footer
			text: qsTr("Please wait...")
			anchors.fill: parent
			verticalAlignment: Text.AlignVCenter
			horizontalAlignment: Text.AlignLeft
		}
	}

	SwipeView {
		id: swipe
		anchors.fill: parent
		currentIndex: 0

		function setCurrentIndex( index ) {
			currentIndex = index
		}

		onCurrentIndexChanged: {
			if ( currentIndex == 1 )
				detailsPage.refresh()
			//console.log( currentIndex )
		}

		// first swipe item
		ListView {
			id: viewGroup
			model: modelGroup
			//anchors.fill: parent
			spacing: 4
			//highlight: Rectangle { color: "red"; radius: 5 }
			//focus: true

			delegate: Rectangle {
				width: viewGroup.width
				Layout.fillWidth: true
				height: textName.implicitHeight
				//color: "gray"
				//height: 40

				RowLayout {
					spacing: 3
					anchors.fill: parent
					//color: "gray"

					Rectangle {
						width: viewGroup.width / 100. * 60.
						radius: 3
						//color: bgcolor
						gradient: Gradient {
							GradientStop {
								position: 0.0
								color: Qt.lighter( bgcolor, 1.05 )
							}
							GradientStop {
								position: 1.0
								color: Qt.darker( bgcolor, 1.2 )
							}
						}

						Layout.fillWidth: true
						Layout.fillHeight: true

						Text {
							id: textName
							text: name
							anchors.fill: parent
							horizontalAlignment: Text.AlignLeft
							verticalAlignment: Text.AlignVCenter
							leftPadding: 16
							topPadding: 10
							bottomPadding: 10
							font.pixelSize: 16
							wrapMode: Text.Wrap
						}
					}

					Rectangle {
						width: viewGroup.width / 100. * 35.
						radius: 3
						//color: bgpower
						gradient: Gradient {
							GradientStop {
								position: 0.0
								color: Qt.lighter( bgpower, 1.05 )
							}
							GradientStop {
								position: 1.0
								color: Qt.darker( bgpower, 1.2 )
							}
						}
						Layout.fillWidth: true
						Layout.fillHeight: true

						Text {
							text: power
							anchors.fill: parent
							horizontalAlignment: Text.AlignRight
							verticalAlignment: Text.AlignVCenter
							rightPadding: 5
						}
					}

					Rectangle {
						width: viewGroup.width / 100. * 15.
						//color: bgtemperature
						radius: 3
						gradient: Gradient {
							GradientStop {
								position: 0.0
								color: Qt.lighter( bgtemperature, 1.05 )
							}
							GradientStop {
								position: 1.0
								color: Qt.darker( bgtemperature, 1.2 )
							}
						}
						Layout.fillWidth: true
						Layout.fillHeight: true

						Text {
							text: temperature
							anchors.fill: parent
							horizontalAlignment: Text.AlignRight
							verticalAlignment: Text.AlignVCenter
							rightPadding: 5
						}
					}
				}

				MouseArea {
					z: 1
					anchors.fill: parent
					onClicked: {
						detailsActivate( index )
					}
				}
			}

			/*
			delegate: Rectangle {
				width: viewGroup.width
				anchors.left: parent.left
				Text {
					//color: modelData.color
					width: parent.width
					horizontalAlignment: Text.AlignHCenter
					text: modelData
					font.pixelSize: 24
				}
				color: "red"

				Repeater {
					model: modelGroup
					delegate: Text {
						verticalAlignment: Text.AlignVCenter
						renderType: Text.NativeRendering
						text: model.color
					}
				}
			}
			*/
			BusyIndicator {
				id: busy
				anchors.centerIn: parent
			}
		}

		// second swipe item
		Details {
			id: detailsPage
		}


		// Third swipe item
		PlacesMap {
			id: map
			center {
				latitude:53.925731
				longitude:37.654179
			}
			zoomLevel: 8.5
			minimumZoomLevel:7
			maximumZoomLevel:10
			activeMapType: map.supportedMapTypes[7]

			/*
			MouseArea {
				z: 3
				anchors.fill: parent
				onDoubleClicked: {
					swipe.currentIndex = 1
					console.log("d clicked" + swipe.currentIndex )
				}
			}
			*/
		}
	}		// SwipeView

    Popup {
        id: settingsPopup
        x: ( window.width - width ) / 2
        y: window.height / 6
        width: Math.min( window.width, window.height )
        height: settingsColumn.implicitHeight + topPadding + bottomPadding
        modal: true
        focus: true

        contentItem: ColumnLayout {
            id: settingsColumn
            spacing: 20

            Label {
                text: qsTr("Settings")
                font.bold: true
            }

			GridLayout {
				columns: 2
				// row 1
                Label {
                    text: qsTr("URL")
					Layout.fillWidth: true
					horizontalAlignment: Text.AlignRight		// не работает (((
                }
                TextField {
                    id: fieldUrl
                    text: settings.url
                    placeholderText: qsTr("type URL here")
                }
				// row 2
				Label {
					width: parent.width
					horizontalAlignment: Text.AlignRight		// не работает (((
					text: qsTr("User")

				}

				TextField {
					id: fieldUser
					text: settings.username
					//placeholderText: qsTr("username for connect")
				}
				// row 3
				Label {
					text: qsTr("Password")
					Layout.fillWidth: true
					horizontalAlignment: Text.AlignRight		// не работает (((
				}

				TextField {
					id: fieldPassword
					text: settings.password
					echoMode: TextInput.PasswordEchoOnEdit
				}
				// row 4
                Label {
                    text: qsTr("Interval")
					Layout.fillWidth: true
					horizontalAlignment: Text.AlignRight		// не работает (((
                }

                SpinBox {
                    id: spinInterval
                    value: settings.interval
                    from: 20
                    to: 300
                }
			}

            RowLayout {
                spacing: 10

                Button {
                    id: buttonOk
                    text: qsTr("OK")
                    onClicked: {
                        settings.url = fieldUrl.text
                        settings.interval = spinInterval.value
						settings.username = fieldUser.text
						settings.password = fieldPassword.text
                        settingsPopup.close()
                    }
                    Layout.preferredWidth: 0
                    Layout.fillWidth: true
                }

                Button {
                    id: buttonClose
                    text: qsTr("Close")
                    onClicked: {
                        settingsPopup.close()
                    }
                }
            }
        }
    }
	/** \brief Диагностические и отладочные сообщения
	 *
	 * \sa addMessage( msg )
	 */
	Popup {
		id: messages
        x: 0
        y: 0
        width: window.width
        height: window.height - toolBar.height - statusBar.height
        modal: true
        focus: true

		Flickable {
			anchors.fill: parent

			TextArea.flickable: TextArea {
				id: messagesText
				textFormat: TextEdit.RichText
				wrapMode: TextArea.Wrap
				readOnly: true
			}

			ScrollBar.vertical: ScrollBar {}
		}
	}
	/*
	Popup {
		id: details
        x: 0
        y: - toolBar.height
        width: window.width
        height: window.height - statusBar.height// - toolBar.height
        modal: true
        focus: true

		Details {
			id: detailsPage
			anchors.fill: parent
		}
	}
		*/

	/*
	Popup {
		id: detailsPopup
        x: 0
        y: 0
        width: window.width
        height: window.height - toolBar.height - statusBar.height
        modal: true
        focus: true

		Flickable {
			anchors.fill: parent

			TextArea.flickable: TextArea {
				id: detailsText
				wrapMode: TextArea.Wrap
				readOnly: true
				textformat: TextEdit.RichText
			}

			ScrollBar.vertical: ScrollBar {}
		}
	}
	*/

	Popup {
		id: aboutDialog
		x: 10
		y: 10
		width: window.width - 20
		height: window.height - toolBar.height - 20

		TextArea {
			anchors.fill: parent
			readOnly: true
			textFormat: TextEdit.RichText
			text: "© 2016 «EcoVolt»<br>" +
			"совместно с<br>" +
			"«АРД сатком сервис»<br><br>" +
			"<b>RTRS mobile client</b>" +
			"<hr>" +
			"e-mail: <a href='mailto:info@eco-volt.net'>info@eco-volt.net</a>"

		}
	}

	/*
	Popup {
		id: popupMap
		x: 0
		y: 0
		width: window.width
		height: window.height - toolBar.height - statusBar.height
		padding: 0

		PlacesMap {
			id: map
		}
	}
		*/
}


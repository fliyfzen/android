/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtPositioning 5.5
import QtLocation 5.6
import QtGraphicalEffects 1.0

Item {
	id: item
	width: 25
	height: 32

	property string text: "text"
	property string color: "blue"

	signal clicked()

	Rectangle {
		id: rect
		color: parent.color
		anchors.fill: parent
		visible: false
	}

	Image {
		id: dropMask
		source: "drop_mask.png"
		visible: false
	}

	OpacityMask {
		anchors.fill: parent
		source: rect
		maskSource: dropMask
	}

	Image {
		source: "drop_outline.png"
	}


	Text {
		font.pixelSize: 9
		anchors.centerIn: parent
		text: parent.text

		horizontalAlignment: Text.AlignHCenter

		MouseArea {
			anchors.fill: parent
			onClicked: item.clicked()
		}

	}

}

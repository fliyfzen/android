/******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
//import Qt.labs.settings 1.0

import online.ecovolt.qmlapplication 1.1

Page {
	id: page
	font.pixelSize: 11

	property int modelIndex: 0

	function setTabIndex( index ) {
		tabBar.currentIndex = index
	}

	function refresh( index ) {
		// cохранить для обновления

		if ( typeof index !== 'undefined' ) {
			modelIndex = index;
		}

		function get( name ) {
			return QmlApplication.getModelHostData( modelIndex, name );
		}

		headerLabel.text = get("name");

		////// Параметры
		// Пинг
		paramPing.value = get("ping")
		paramPing.color = get("bgagentping")
		// Дверь
		paramDoor.value = get("door");
		paramDoor.color = get("bgdoor");
		// Движение
		paramMoving.value = get("moving");
		paramMoving.color = get("bgmoving");
		// Пожар
		paramFire.value = get("fire");
		paramFire.color = get("bgfire");
		// Температура
		paramTemperature.value = get("temperature");
		paramTemperature.color = get("bgtemperature");
		// Кондиционер 1
		paramCond1.value = get("cond1");
		paramCond1.color = get("bgcond1");
		// Кондиционер 2
		paramCond2.value = get("cond2");
		paramCond2.color = get("bgcond2");
		// Мощность
		paramPower.value = get("power")
		paramPower.color = get("bgpower")
		// КВСН
		paramKsvn.value = get("ksvn");
		paramKsvn.color = get("bgksvn");
		// DSP передатчика
		paramTrsDsp.value = get("trsdsp");
		paramTrsDsp.color = get("bgtrsdsp");
		// Температура передатчика
		paramTrsTemp.value = get("trstemp");
		paramTrsTemp.color = get("bgtrstemp");
		// TS передатчика
		paramTrsTs.value = get("trsts");
		paramTrsTs.color = get("bgtrsts");
		// TX статус передатчика
		paramTrsTx.value = get("trstx");
		paramTrsTx.color = get("bgtrstx");
		// DUC state
		paramDucState.value = get("ducstate");
		paramDucState.color = get("bgducstate");
		// DUC температура
		paramDucTemp.value = get("ductemp");
		paramDucTemp.color = get("bgductemp");
		// Mod state
		paramModState.value = get("modstate");
		paramModState.color = get("bgmodstate");
		// Mod temperature
		paramModTemp.value = get("modtemp");
		paramModTemp.color = get("bgmodtemp");
		// Отражённая можность
		paramPowerRef.value = get("powerref");
		paramPowerRef.color = get("bgpowerref");
		// Eb/N0
		paramEbN0.value = get("ebn0");
		paramEbN0.color = get("bgebn0");
		// log(BER)
		paramLogBer.value = get("logber");
		paramLogBer.color = get("bglogber");
		// log(PER)
		paramLogPer.value = get("logper");
		paramLogPer.color = get("bglogper");
		// Запас устойчивости связи
		paramZapUs.value = get("zapust");
		paramZapUs.color = get("bgzapust");
		// Захвате демодулятора
		paramZahDemod.value = get("zahdemod");
		paramZahDemod.color = get("bgzahdemod");
		// Захват сигнала
		paramZahSignal.value = get("zahsignal");
		paramZahSignal.color = get("bgzahsignal");
		// Отношение несущая/шум
		paramShum.value = get("shum");
		paramShum.color = get("bgshum");
		// UPS U in
		paramUPSUIn.value = get("upsuin");
		paramUPSUIn.color = get("bgupsuin");
		// UPS U out
		paramUPSUOut.value = get("upsuout");
		paramUPSUOut.color = get("bgupsuout");
		// UPS Power
		paramUPSPower.value = get("upspower");
		paramUPSPower.color = get("bgupspower");
		// UPS Load
		paramUPSLoad.value = get("upsload");
		paramUPSLoad.color = get("bgupsload");
		// Счётчик, значение от сброса
		paramTotal.value = get("total");
		paramTotal.color = get("bgtotal")
		// Счётчик, активная мощность
		paramActive.value = get("active")
		paramActive.color = get("bgactive")
		// Счётчик, реактивная мощность
		paramReactive.value = get("reactive")
		paramReactive.color = get("bgreactive")

		textHistory.text = get("errorshistory");
	}

	header: RowLayout {
		anchors.margins: 10
		spacing: 5
		Layout.fillWidth: true
		Text {
			id: headerLabel
			horizontalAlignment: Qt.AlignRight
			font.pixelSize: 16
			//Layout.fillWidth: true
		}
		Param {
			Layout.fillWidth: false
			width: 150
			id: paramPing
			text: qsTr("ping")
		}
	}

	StackLayout {
		id: stack
		anchors.fill: parent
		currentIndex: tabBar.currentIndex

		/** \brief Params
		 */
		Flickable {
			anchors.fill: parent
			contentWidth: paramsRect.width
			contentHeight: lastItem.y
			clip: true

			Rectangle {
				id: paramsRect
				//anchors.fill: parent
				width: stack.width
				height: 700			// заведомо большое число, реальная высота == lastItem.y
				color: "lightblue"

				ColumnLayout {
					id: paramsLayout
					anchors.fill: parent
					spacing: 0
					// Дверь, датчик движения, пожар
					RowLayout {
						Layout.alignment: Qt.AlignTop
						Layout.margins: 3
						//Layout.fillHeight: true
						Param {
							id: paramDoor
							text: qsTr("Door")
							withoutValue: true
						}
						Param {
							id: paramMoving
							text: qsTr("Moving")
							withoutValue: true
						}
						Param {
							id: paramFire
							text: qsTr("Fire")
							withoutValue: true
						}
					}
					// Температура, Кондиционер 1, Кондиционер 2
					RowLayout {
						Layout.alignment: Qt.AlignTop
						Layout.margins: 3
						//Layout.fillHeight: false
						Param {
							id: paramTemperature
							text: qsTr("Temperature")
						}
						Param {
							id: paramCond1
							text: qsTr("Cond 1")
						}
						Param {
							id: paramCond2
							text: qsTr("Cond 2")
						}
					}

					GridLayout {		// buttonTransmitter
						id: gridButtonTransmitter
						columns: 3
						Layout.margins: 3
						width: parent.width
						ButtonMoreLess {
							id: buttonTransmitter

							onClicked: {
								if ( checked ) {
									buttonReceiver.checked = false
									buttonUps.checked = false
									buttonCounter.checked = false
								}
							}
						}
						Label {
							text: qsTr("Transmitter")
							Layout.columnSpan: 2
							Layout.fillWidth: true
							horizontalAlignment: Qt.AlignHCenter
							font.bold: true
						}
						Param {
							id: paramPower
							text: qsTr("Output power")
						}
						Param {
							id: paramTrsTemp
							text: qsTr("Temperature")
						}
					}

					// показывается при необходимости
					GridLayout {
						id: gridTransmitter
						visible: buttonTransmitter.checked
						columns: 2
						Layout.margins: 3
						width: parent.width
						Param {
							id: paramPowerRef
							text: qsTr("Отраж. мощность")
							//width: parent.width / 2
						}
						Param {
							id: paramKsvn
							text: qsTr("КСВН")
						}
						Param {
							id: paramTrsDsp
							text: qsTr("DSP")
						}
						Param {
							id: paramTrsTs
							text: qsTr("TS")
						}
						Param {
							id: paramTrsTx
							text: qsTr("TX state")
						}
						Item { /// STUB
							Layout.fillWidth: true
						}
						Param {
							id: paramDucState
							text: qsTr("DUC state")
						}
						Param {
							id: paramDucTemp
							text: qsTr("DUC temp")
						}
						Param {
							id: paramModState
							text: qsTr("Mod state")
						}
						Param {
							id: paramModTemp
							text: qsTr("Mod temp")
						}
					}


					GridLayout {		// buttonReceiver
						id: gridButtonReceiver
						columns: 3
						Layout.margins: 3
						width: parent.width
						ButtonMoreLess {
							id: buttonReceiver

							onClicked: {
								if ( checked ) {
									buttonTransmitter.checked = false
									buttonUps.checked = false
									buttonCounter.checked = false
								}
							}
						}
						Label {
							text: qsTr("Receiver")
							Layout.columnSpan: 2
							Layout.fillWidth: true
							horizontalAlignment: Qt.AlignHCenter
							font.bold: true
						}
						Param {
							id: paramEbN0
							text: qsTr("Eb/N0")
						}
						Param {
							id: paramZahSignal
							text: qsTr("Захват сигнала")
						}
					}

					GridLayout {
						id: gridReceiver
						visible: buttonReceiver.checked
						columns: 2
						Layout.margins: 3
						width: parent.width
						Param {
							id: paramLogBer
							text: qsTr("log(BER)")
						}
						Param {
							id: paramLogPer
							text: qsTr("log(PER)")
						}
						Param {
							id: paramZapUs
							text: qsTr("Запас устойчивости связи")
						}
						Param {
							id: paramZahDemod
							text: qsTr("Захват демодулятора")
						}
						Param {
							id: paramShum
							text: qsTr("Отношение несущая/шум")
						}
					}

					GridLayout {		// buttonUps
						id: gridButtonUps
						columns: 3
						Layout.margins: 3
						width: parent.width
						ButtonMoreLess {
							id: buttonUps

							onClicked: {
								if ( checked ) {
									buttonReceiver.checked = false
									buttonTransmitter.checked = false
									buttonCounter.checked = false
								}
							}
						}
						Label {
							text: qsTr("UPS")
							Layout.columnSpan: 2
							Layout.fillWidth: true
							horizontalAlignment: Qt.AlignHCenter
							font.bold: true
						}

						Param {
							id: paramUPSUOut
							text: qsTr("U out")
						}
						Param {
							id: paramUPSLoad
							text: qsTr("Загрузка")
						}
					}

					GridLayout {
						id: gridUps
						visible: buttonUps.checked
						columns: 2
						Layout.margins: 3
						width: parent.width
						Param {
							id: paramUPSUIn
							text: qsTr("U in")
						}
						Param {
							id: paramUPSPower
							text: qsTr("Нагрузка")
						}
					}

					GridLayout {
						id: gridButtonCounter
						columns: 3
						Layout.margins: 3
						width: parent.width
						ButtonMoreLess {
							id: buttonCounter

							onClicked: {
								if ( checked ) {
									buttonReceiver.checked = false
									buttonTransmitter.checked = false
									buttonUps.checked = false
								}
							}
						}
						Label {
							text: qsTr("Counter")
							Layout.columnSpan: 2
							Layout.fillWidth: true
							horizontalAlignment: Qt.AlignHCenter
							font.bold: true
						}
						Param {
							id: paramTotal
							text: qsTr("Показания")
						}
						Param {
							id: paramActive
							text: qsTr("Нагрузка А")	// Активная
						}
					}

					GridLayout {
						id: gridCounter
						visible: buttonCounter.checked
						columns: 2
						Layout.margins: 3
						width: parent.width
						Param {
							id: paramReactive
							text: qsTr("Нагрузка Р")	// Реактивная
						}
					}

					/// РАЗДВИГАЮЩИЙ ITEM
					Item {
						id: lastItem
						Layout.fillHeight: true
					}
				}
			}

			// Рамка для Transmission
			Rectangle {
				x: 0
				y: gridButtonTransmitter.y
				width: parent.width
				height: gridButtonTransmitter.height + 2 + ( gridTransmitter.visible ? gridTransmitter.height + 7 : 0 )
				color: "transparent"
				border.color: "#555555"
				border.width: 1
				radius: 5
			}

			// Рамка для Receiver
			Rectangle {
				x: 0
				y: gridButtonReceiver.y
				width: parent.width
				height: gridButtonReceiver.height + 2 + ( gridReceiver.visible ? gridReceiver.height + 7 : 0 )
				color: "transparent"
				border.color: "#555555"
				border.width: 1
				radius: 5
			}

			// Рамка для UPS
			Rectangle {
				x: 0
				y: gridButtonUps.y
				width: parent.width
				height: gridButtonUps.height + 2 + ( gridUps.visible ? gridUps.height + 7 : 0 )
				color: "transparent"
				border.color: "#555555"
				border.width: 1
				radius: 5
			}

			// Рамка для Counter
			Rectangle {
				x: 0
				y: gridButtonCounter.y
				width: parent.width
				height: gridButtonCounter.height + 2 + ( gridCounter.visible ? gridCounter.height + 7 : 0 )
				color: "transparent"
				border.color: "#555555"
				border.width: 1
				radius: 5
			}


		}	// Flickable for Params

		/** \brief Errors history
		 */
		Flickable {
			anchors.fill: parent

			TextArea.flickable: TextArea {
				id: textHistory
				color: "black"
				textFormat: TextEdit.RichText
				wrapMode: TextArea.Wrap
				readOnly: true
				font.pixelSize: 11
			}

			ScrollBar.vertical: ScrollBar {}
		}

		/** \brief Charts
		 */
		Rectangle {
			color: "beige"
		}
	}

	footer: TabBar {
		id: tabBar
		currentIndex: stack.currentIndex

		TabButton {
			text: qsTr("Params")
		}
		TabButton {
			text: qsTr("Errors history")
		}
		TabButton {
			text: qsTr("Charts")
		}
	}
}


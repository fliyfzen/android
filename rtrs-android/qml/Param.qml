/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.0

Rectangle {
	id: outerRect
	height: label.implicitHeight
	radius: 6
	border.width: 1
	antialiasing: true
	border.color: "#666666"
	Layout.fillWidth: true
	//color: valueColor

	property string text
	property string value: "0.00"
	property string color: "gray"
	property bool withoutValue: false

	gradient: Gradient {
		GradientStop {
			position: 0.0
			color: withoutValue ? ( value == "???" ? "lightgray" : Qt.lighter( outerRect.color, 1.05 ) ) :
				( value == "???" ? "lightgray" : Qt.darker( outerRect.color, 1.2 ) )
		}
		GradientStop {
			position: 1.0
			color: withoutValue ? ( value == "???" ? "lightgray" : Qt.darker( outerRect.color, 1.2 ) ) :
				( value == "???" ? "lightgray" : Qt.lighter( outerRect.color, 1.05 ) )
		}
	}


	RowLayout {
		anchors.fill: parent
		Label {
			leftPadding: 5
			topPadding: 5
			bottomPadding: 5
			rightPadding: -4
			width: outerRect.width / 100. * 60
			color: value == "???" ? "gray" : "black"
			//Layout.fillWidth: true
			id: label
			text: outerRect.text
			//elide: Text.ElideRight
		}

		Rectangle {
			radius: 6
			Layout.fillWidth: true
			Layout.fillHeight: true
			Layout.minimumWidth: 5
			border.width: 1
			//width: label.implicitWidth
			border.color: "#666666"
			//color: outerRect.color
			gradient: Gradient {
				GradientStop {
					position: 0.0
					color: value == "???" ? "lightgray" : Qt.lighter( outerRect.color, 1.05 )
				}
				GradientStop {
					position: 1.0
					color: value == "???" ? "lightgray" : Qt.darker( outerRect.color, 1.2 )
				}
			}

			visible: ! outerRect.withoutValue

			Label {
				id: labelValue
				anchors.fill: parent
				text: outerRect.value
				color: value == "???" ? "gray" : "black"
				horizontalAlignment: Qt.AlignHCenter
				verticalAlignment: Qt.AlignVCenter
			}
		}
	}
}

/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

import QtQuick 2.7
import QtPositioning 5.5
import QtLocation 5.6
import QtQuick.Controls 2.0

Map {
	id: map

	function updateMapItem( name, color, dropText ) {
		var mi = mapItems

		for ( var i = 0; i < mi.length; ++i ) {
			if ( mi[ i ].text == name ) {
				mi[ i ].color = color
				mi[ i ].dropText = dropText
				break;
			}
		}
	}

	plugin: Plugin {
        name: "osm"
        preferred: ["osm"]
        allowExperimental: true
        PluginParameter { name: "osm.mapping.host"; value: "http://a.tile.openstreetmap.org/"}
    }

	Rectangle {			// Zoom in
		width: 50
		height: 50
		x: map.width - width - 10
		y: 10
		z: 10
		radius: 5
		color: "#88ffffff"
		Text {
			anchors.centerIn: parent
			text: "+"
		}

		MouseArea {
			anchors.fill: parent
			onClicked: {
				map.zoomLevel += .4
			}
		}
	}

	Rectangle {			// Zoom out
		width: 50
		height: 50
		x: map.width - width - 10
		y: 70
		z: 10
		radius: 5
		color: "#88ffffff"
		Text {
			anchors.centerIn: parent
			text: "-"
		}

		MouseArea {
			anchors.fill: parent
			onClicked: {
				map.zoomLevel -= .4
			}
		}
	}


	Rectangle {			// Move to origin
		width: 50
		height: 50
		x: map.width - width - 10
		y: 130
		z: 10
		radius: 5
		color: "#88ffffff"
		Text {
			anchors.centerIn: parent
			text: "⊙"
		}

		MouseArea {
			anchors.fill: parent
			onClicked: {
				center.latitude = 53.925731
				center.longitude = 37.654179
			}
		}
	}
}


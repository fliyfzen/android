################################################################################
#               __________                               __________            #
#               ___  ____/___________       ___   __________  /_  /_           #
#               __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           #
#               _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             #
#               /_____/  \___/ \____/       _____/ \____//_/  \__/             #
#                                                                              #
#     © 2016                                                                   #
#                                                                              #
################################################################################

TARGET = rtrs-android
CONFIG += c++11
QT += qml quick positioning

win32 {
INCLUDEPATH += ..//zbx//src

CONFIG(release, debug|release): LIBS += -L..//zbx//release// -lzbx

else:CONFIG(debug, debug|release): LIBS += -L..//zbx//debug// -lzbx

}

INCLUDEPATH += ../zbx/src

LIBS += -L../zbx -lzbx

HEADERS += src/Block.h \
		   src/QmlApplication.h

SOURCES += src/main.cpp \
		   src/Block.cpp \
		   src/QmlApplication.cpp

lupdate_only {
	SOURCES += qml/*.qml
}

RESOURCES += qml.qrc \
			 images.qrc \
			 translations.qrc

LANGUAGES = ru en
# Create .ts files
defineReplace( prependAll ) {
	for( a, $$1 ):result += $$2$${a}$$3
	return( $$result )
}

TRANSLATIONS += translations/rtrs-android_ru.qm

tsroot = $$join( TARGET,,,.ts )
tstarget = $$join( TARGET,,,_ )
TRANSLATIONS = $$PWD/translations/$$tsroot
TRANSLATIONS += $$prependAll( LANGUAGES, $$PWD/translations/$$tstarget, .ts )

qtPrepareTool( LRELEASE, lrelease )
for( tsfile, TRANSLATIONS ) {
	command = $$LRELEASE $$tsfile
	system( $$command ) | error("Failed to run: $$command")
}

include(../customer.pri)

# Additional import path used to resolve QML modules in Qt Creator`s code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android


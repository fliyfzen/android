/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#include <QDir>
#include <QGuiApplication>
#include <QTranslator>

#include "QmlApplication.h"

QObject *
singleton( QQmlEngine * engine = nullptr, QJSEngine * scriptEngine = nullptr )
{
	Q_UNUSED( engine )
	Q_UNUSED( scriptEngine )

	static QObject * o_ptr = nullptr;

	if ( o_ptr == nullptr )
		o_ptr = new QmlApplication;

	return o_ptr;
}

int
main( int argc, char ** argv )
{
    QCoreApplication::setApplicationName("rtrs-android");
    QCoreApplication::setOrganizationName("eco-volt");

    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
    QGuiApplication app( argc, argv );

	QTranslator * trans_ru = new QTranslator( &app ),
				* zbx_ru = new QTranslator( &app );

	if ( trans_ru->load(":/rtrs-android_ru.qm") )
		app.installTranslator( trans_ru );

	if ( zbx_ru->load(":/zbx_ru.qm") )
		app.installTranslator( zbx_ru );

	qmlRegisterSingletonType< QmlApplication >("online.ecovolt.qmlapplication", 1, 1, "QmlApplication", singleton );

    qobject_cast< QmlApplication * >( singleton() )->load( QUrl( QLatin1String("qrc:/main.qml") ) );

    return app.exec();
}


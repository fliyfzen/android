/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#ifndef QMLAPPLICATION_H
#define QMLAPPLICATION_H

#include <QQmlApplicationEngine>

class QGeoPositionInfo;
class QTimer;
class ModelError;
class ModelHost;
class Zbx;

/** \brief 
 */
class QmlApplication : public QQmlApplicationEngine
{
	Q_OBJECT

	private:
		Zbx * m_zbx;

		ModelHost * m_modelHost;

		ModelError * m_modelError;

		QTimer * m_timerRefresh;

		/** \brief Добавляет \a message в сообщения
		 */
		void qmlLog( const QString & message );

		QObject * rootObject();

		/** \brief Для отладки
		 */
		void saveToFile( const QJsonObject & object, const QString & fileName = "out.json") const;

		void createMapItems();

		void updateMapItems();

	private Q_SLOTS:
		void afterLogin();
		void processData( const QJsonObject & object );
		void refresh();		///< Обновляет все данные по таймеру
		void refreshModelHost();	///< Обновляет хосты, при запуске приложения

		void geoPositionUpdated( const QGeoPositionInfo & geoPosInfo );

	public:
		QmlApplication( QObject * parent = nullptr );

		void load( const QUrl & url );

		/** \brief Вызывается из QML
		 */
		Q_INVOKABLE int getModelIndexByErrorIndex( int errorIndex );

		/** \brief Вызывается из QML
		 */
		Q_INVOKABLE QString getHostInfoByHostId( const QString & hostId ) const;

		Q_INVOKABLE QVariant getModelHostData( int index, const QString & roleName ) const;
};

#endif

/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#include "QmlApplication.h"

#include <QtQml>
#include <QtPositioning>

#include <QJsonArray>
#include <QJsonObject>

// from zbx
#include <ModelError.h>
#include <ModelHost.h>
#include <Zbx.h>

#include "Block.h"

QmlApplication::QmlApplication( QObject * parent )
	: QQmlApplicationEngine( parent )
{
	QSettings s;
	m_zbx = new Zbx( this );

	m_modelHost = new ModelHost( this );

	m_modelError = new ModelError( this );

	QGeoPositionInfoSource * geo_pos = QGeoPositionInfoSource::createDefaultSource( this );
	if ( geo_pos ) {
		connect( geo_pos, SIGNAL( positionUpdated( const QGeoPositionInfo & ) ),
				SLOT( geoPositionUpdated( const QGeoPositionInfo & ) ) );

		geo_pos->startUpdates();
	}


	QQmlContext * ctxt = rootContext();
	ctxt->setContextProperty("modelGroup", QVariant::fromValue( m_modelHost ) );
	ctxt->setContextProperty("modelError", QVariant::fromValue( m_modelError ) );

	connect( m_zbx, SIGNAL( loginOk() ), SLOT( afterLogin() ) );
	connect( m_zbx, SIGNAL( dataReady( const QJsonObject & ) ), SLOT( processData( const QJsonObject & ) ) );

	m_timerRefresh = new QTimer( this );
	m_timerRefresh->setInterval( 30000 );	// 30 sec.
	connect( m_timerRefresh, SIGNAL( timeout() ), SLOT( refresh() ) );
	m_timerRefresh->start();

	refresh();
}

void
QmlApplication::load( const QUrl & url )
{
	QQmlApplicationEngine::load( url );

	connect( m_zbx, SIGNAL( message( const QVariant & ) ), rootObject(), SLOT( addMessage( const QVariant & ) ) );
}

QObject *
QmlApplication::rootObject()
{
	QList< QObject * > roots = rootObjects();
	QObject * obj = roots.isEmpty() ? nullptr : roots.first();

	if ( obj == nullptr )
		qDebug() << "No root objects for qmlLog()";

	return obj;
}

void
QmlApplication::refresh()		///< Обновляет все данные по таймеру
{
	if ( ! m_zbx->isOpen() ) {
		QSettings s;
		m_zbx->get( Zbx::Login, QVariantList() << s.value("username") << s.value("password") );
		return;
	}

	refreshModelHost();
}

void
QmlApplication::afterLogin()		// private slot
{
	QMetaObject::invokeMethod( rootObject(), "busyShow",
			Q_ARG( QVariant, QVariant( false ) ) );

	//m_zbx->get( Zbx::Version );

	m_zbx->get( Zbx::HostGroup );

	//refreshModelHost();

	//m_zbx->get( Zbx::CurrentErrors );
	//m_zbx->get( Zbx::Values );
}

void
QmlApplication::processData( const QJsonObject & object )	// private slot
{
	switch( object.value("id").toInt() ) {
		case Zbx::HostGroup: {
			foreach( const QJsonValue & v1, object.value("result").toArray() ) {
				const QJsonObject o1 = v1.toObject();
				//qDebug() << o1.value("hostid").toString();
				foreach( const QJsonValue & v2, o1.value("groups").toArray() ) {
					const QJsonObject o2 = v2.toObject();
					//qDebug() << o2.value("groupid").toString() << o2.value("groupid").toString();
					if ( o2.value("groupid").toString() != REGION_CODE ) {
						Host host;
						host.hostId = o1.value("hostid").toString();
						host.name = o2.value("name").toString();
						if ( host.hostId != ZABBIX_HOST_ID )		// Zabbix не сохранять
							m_modelHost->append( host );
					}
				}
			}

			m_modelHost->sortByName();

			createMapItems();

			refreshModelHost();
			break;
		}

		case Zbx::Values: {
			//saveToFile( object, "Values.json");
			Block block;
			const QJsonArray j_result = object.value("result").toArray();
			foreach( const QJsonValue & value, j_result ) {
				const QJsonObject jo = value.toObject();
				const QString hostId = jo.value("hostid").toString(),
							  name = jo.value("name").toString(),
							  itemId = jo.value("itemid").toString(),
							  lastvalue = jo.value("lastvalue").toString();

				qApp->processEvents();	// чтоб не тормозил интерфейс !!!

				m_modelHost->set( hostId, name, ZbxValue( itemId, lastvalue ) );
			}

			updateMapItems();

			break;
		}

		case Zbx::ErrorsHistory: {
			//saveToFile( object, "ErrorsHistory.json");
			m_modelHost->resetHistoryErrors();
			const QJsonArray j_result = object.value("result").toArray();
			foreach( const QJsonValue & value, j_result ) {
				const QJsonObject jo = value.toObject();
				const QString hostId = jo.value("hostid").toString(),
					          priority = jo.value("priority").toString(),
							  description = jo.value("description").toString(),
							  time = jo.value("lastchange").toString(),
							  triggerId = jo.value("triggerid").toString();

				m_modelHost->addHistoryError( hostId, triggerId, priority, description, time );
			}
			/*const QMultiMap< QString, HostError > errors =*/ m_modelHost->sortHistoryErrors();
			break;
		}

		case Zbx::CurrentErrors: {
			//saveToFile( object, "CurrentError.json");
			m_modelHost->resetErrors();
			const QJsonArray j_result = object.value("result").toArray();
			foreach( const QJsonValue & value, j_result ) {
				const QJsonObject jo = value.toObject();
				const QString priority = jo.value("priority").toString(),
							  description = jo.value("description").toString(),
							  time = jo.value("lastchange").toString();
				QString hostId;
				foreach( const QJsonValue & v_host, jo.value("hosts").toArray() ) {
					const QJsonObject host = v_host.toObject();
					if ( host.contains("hostid") ) {
						hostId = host.value("hostid").toString();
						break;
					}
				}
				QString itemId;
				foreach( const QJsonValue & v_item, jo.value("items").toArray() ) {
					const QJsonObject item = v_item.toObject();
					if ( item.contains("itemid") ) {
						itemId = item.value("itemid").toString();
						break;
					}
				}

				m_modelHost->addError( hostId, itemId, priority, description, time );
			}

			m_modelError->update( m_modelHost );
			updateMapItems();
			break;
		}

		default:
			;
	}
}

void
QmlApplication::refreshModelHost()			// private slot
{
	if ( Block::isBlocked() ) {
		QMetaObject::invokeMethod( rootObject(), "setStatusText",
				Q_ARG( QVariant, QVariant( tr("skiped at %1").arg( QTime::currentTime().toString("hh:mm:ss") ) ) ) );
		return;
	}

	QMetaObject::invokeMethod( rootObject(), "busyShow",
			Q_ARG( QVariant, QVariant( true ) ) );

	QVariantList list_hosts = m_modelHost->hostIds();

	m_zbx->get( Zbx::CurrentErrors, list_hosts );
	m_zbx->get( Zbx::Values, list_hosts );
	m_zbx->get( Zbx::ErrorsHistory, list_hosts );

	//updateMapItems();

	//
	// set status message
	//
	QMetaObject::invokeMethod( rootObject(), "setStatusText",
			Q_ARG( QVariant, QVariant( tr("updated at %1").arg( QTime::currentTime().toString("hh:mm:ss") ) ) ) );

	QMetaObject::invokeMethod( rootObject(), "busyShow",
			Q_ARG( QVariant, QVariant( false ) ) );

}

void
QmlApplication::qmlLog( const QString & message )
{
	QMetaObject::invokeMethod( rootObject(), "addMessage",
			//Q_RETURN_ARG( QVariant, rc ),
			Q_ARG( QVariant, QVariant( message ) ) );
}

int
QmlApplication::getModelIndexByErrorIndex( int errorIndex )		// Q_INVOKABLE
{
	if ( m_modelError->count() <= errorIndex ) {
		QMetaObject::invokeMethod( rootObject(), "addMessage",
				//Q_RETURN_ARG( QVariant, rc ),
				Q_ARG( QVariant, QVariant( tr("No error for index %1").arg( errorIndex ) ) ) );
		return -1;
	}

	const QString hostId = m_modelError->at( errorIndex ).hostId;

	for ( int i = 0; i < m_modelHost->count(); ++i )
		if ( m_modelHost->host( i ).hostId == hostId )
			return i;


	return -1;
}

QString
QmlApplication::getHostInfoByHostId( const QString & hostId ) const				// Q_INVOKABLE
{
	for( int i = 0; i < m_modelHost->count(); ++i )
		if ( m_modelHost->host( i ).hostId == hostId ) {
			return tr("<b>%1</b><br><br>"
					"Host ID: %2<br>"
					"Power: %3<br>"
					"Temperature: %4<br>"
					"Errors:<br>%5<br>"
					)
				.arg( m_modelHost->host( i ).name )
				.arg( m_modelHost->host( i ).hostId )
				.arg( m_modelHost->host( i ).power.value )
				.arg( m_modelHost->host( i ).temperature.value )
				.arg( m_modelHost->host( i ).richErrors() );
		}

	return tr("<font color=red>Host with ID=%1 not found.</font>").arg( hostId );
}

void
QmlApplication::saveToFile( const QJsonObject & object, const QString & fileName ) const		// for debug only
{
	QFile out( fileName );

	if ( ! out.open( QIODevice::WriteOnly | QIODevice::Text ) ) {
		qDebug() << "Can't write to " << fileName;
		return;
	}

	out.write( QJsonDocument( object ).toJson() );

	qDebug() << "JSON OBJECT written to file" << fileName;

	out.close();
}

QVariant
QmlApplication::getModelHostData( int index, const QString & roleName ) const
{
	return m_modelHost->data( index, roleName );
}

void
QmlApplication::createMapItems()
{
	for ( int i = 0; i < m_modelHost->count(); ++i ) {
		QMetaObject::invokeMethod( rootObject(), "addMapItem",
				Q_ARG( QVariant, QVariant( i ) ),
				Q_ARG( QVariant, QVariant( m_modelHost->host( i ).name ) ),
				Q_ARG( QVariant, QVariant::fromValue( m_modelHost->host( i ).coordinate() ) ) );
	}
}

void
QmlApplication::updateMapItems()
{
	for ( int i = 0; i < m_modelHost->count(); ++i ) {
		QMetaObject::invokeMethod( rootObject(), "updateMapItem",
				Q_ARG( QVariant, QVariant( m_modelHost->host( i ).name ) ),
				Q_ARG( QVariant, QVariant( m_modelHost->host( i ).errorColor() ) ),
				Q_ARG( QVariant, QVariant( tr("%1\n%2")
						.arg( m_modelHost->host( i ).power.value )
						.arg( m_modelHost->host( i ).temperature.value ) ) ) );
	}

	QMetaObject::invokeMethod( rootObject(), "refreshDetails");
}

void
QmlApplication::geoPositionUpdated( const QGeoPositionInfo & geoPosInfo )
{
	qDebug() << "Geo position" << geoPosInfo;

	qmlLog( tr("Geo position: %1").arg( geoPosInfo.coordinate().toString() ) );
}


/*******************************************************************************
 *              __________                               __________            *
 *              ___  ____/___________       ___   __________  /_  /_           *
 *              __  __/  _  ___/  __ \________ | / /  __ \_  /_  __/           *
 *              _  /___  / /__ / /_/ //_____/_ |/ // /_/ /  / / /_             *
 *              /_____/  \___/ \____/       _____/ \____//_/  \__/             *
 *                                                                             *
 *    © 2016                                                                   *
 *                                                                             *
 ******************************************************************************/

#include "Block.h"

#include <QDebug>

int Block::s_count = 0;

Block::Block()
{
	++s_count;
	//qDebug() << "BLOCKED" << s_count;
}

Block::~Block()			// virtual
{
	--s_count;
	//qDebug() << "UNBLOCKED" << s_count;
}

int
Block::isBlocked()		// static
{
	return s_count;
}

